App.module("RobotsApp.List", function (List, App, Backbone, Marionette, $, _) {
    List.RobotView = Marionette.ItemView.extend({
        tagName: 'tr',
        template: '#robot-item'
    });

    List.RobotsView = Marionette.CompositeView.extend({
        tagName: 'div',
        className: 'row',
        template: '#robot-list',
        childView: List.RobotView,
        childViewContainer: 'tbody',

        events: {
            'click .find': 'onSearch'
        },

        triggers: {
            'click .add': 'robots:add'
        },

        onSearch: function (e) {
            this.trigger('robots:search', $("#search-text").val());
        }
    });
});