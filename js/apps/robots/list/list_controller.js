App.module("RobotsApp.List", function (List, App, Backbone, Marionette, $, _) {
    var Controller = Marionette.Controller.extend({
        listRobots: function (filter) {
            var self = List.Controller;
            var fetchingRobots = App.request("robot:list", filter);

            $.when(fetchingRobots).done(function (robots) {
                var view = new List.RobotsView({collection: robots, model: new List.VM(filter)});
                App.mainRegion.show(view);

                self.listenTo(view, 'robots:search', function (data) {
                    App.trigger('robots:search', data);
                });

                self.listenTo(view, 'robots:add', function (data) {
                    App.trigger('robots:new', data);
                });
            });
        }
    });
    List.Controller = new Controller();
});