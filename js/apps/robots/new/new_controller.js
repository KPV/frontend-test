App.module("RobotsApp.New", function (New, App, Backbone, Marionette, $, _) {
    var Controller = Marionette.Controller.extend({
        addUser: function () {
            var robot = new App.Entities.Robot();
            var view = new New.View.Robot({model: robot});
            App.mainRegion.show(view);

            this.listenTo(view, 'cancel:update', function () {
                App.trigger('robots:list');
            });

            this.listenTo(view, 'form:submit', function (data) {
                var saving = robot.save(data);

                $.when(saving).done(function () {
                    App.trigger('robots:list');
                }).fail(function (response) {
                });

            });
        }
    });
    New.Controller = new Controller();
});