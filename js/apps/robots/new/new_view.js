App.module("RobotsApp.New.View", function (View, App, Backbone, Marionette, $, _) {
    View.Robot = App.RobotsApp.Common.View.RobotForm.extend({
        onRender: function () {
            this.$(".edit-title").text("Добавить робота");
        }
    });
});