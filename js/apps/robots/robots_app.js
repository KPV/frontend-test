App.module("RobotsApp", function (RobotsApp, App, Backbone, Marionette, $, _) {
    RobotsApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            "": "list",
            "robots": "list",
            "robots/search/": "list",
            "robots/search/:name": "list",
            "robots/new": "newRobot"
        }
    });

    var API = {
        list: function (name) {
            RobotsApp.List.Controller.listRobots({name: name});
        },

        newRobot: function () {
            RobotsApp.New.Controller.addUser();
        }
    };

    this.listenTo(App, 'robots:list', function () {
        Backbone.history.navigate('/robots', {trigger: true});
    });

    this.listenTo(App, 'robots:search', function (searchData) {
        Backbone.history.navigate('/robots/search/' + searchData, {trigger: true});
    });

    this.listenTo(App, 'robots:new', function () {
        Backbone.history.navigate('/robots/new', {trigger: true});
    });

    App.addInitializer(function () {
        new RobotsApp.Router({
            controller: API
        });
    });
});