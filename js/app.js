var App = new Marionette.Application();

App.ROOT = "http://frontend.test.pleaple.com/api";

App.addRegions({
    'mainRegion': '#container'
});

App.on("start", function () {
    if (Backbone.history) {
        Backbone.history.start();
    }
});
