App.module("RobotsApp.Common.View", function (View, App, Backbone, Marionette, $, _) {
    View.RobotForm = Marionette.ItemView.extend({
        tagName: 'div',
        template: '#robot-edit',
        triggers: {
            "click .cancel": "cancel:update"
        },

        events: {
            'submit form': 'submitClicked'
        },

        submitClicked: function (e) {
            e.preventDefault();
            var data = {};

            this.$("input[type='text'], select").each(function () {
                data[$(this).attr("name")] = $(this).val();
            });

            this.trigger('form:submit', data);
        }
    });
});
