App.module("Entities", function (Entities, App, Backbone, Marionette, $, _) {

    Entities.Robot = Backbone.Model.extend({
        urlRoot: App.ROOT + "/robots",
        defaults: {
            name: '',
            type: '',
            year: ''
        },

        /*
         * Так как мы юзаем CORS то при сложных запросах (contentType:application/json) браузер сначала шлет запрос с типом OPTIONS
         * По всей видимости сервер про такие запросы не знает и крашится
         * Пришлось переделать запрос на простой POST
         * */
        sync: function (method, model, options) {
            if (method == "create") {
                $.ajax({
                    url: model.url(),
                    type: "POST",
                    data: JSON.stringify(model.attributes),
                    success: options.success,
                    error: options.error
                });
            }
        }
    });

    Entities.Robots = Backbone.Collection.extend({
        model: Entities.Robot,

        url: function () {
            return App.ROOT + '/robots' + this.filter;
        }
    });

    var API = {
        getRobots: function (filter) {
            App.request("robots:filter");
            var robots = new Entities.Robots();
            robots.filter = filter.name != null ? "/search/" + filter.name : "";
            var defer = $.Deferred();
            robots.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        }
    };

    App.reqres.setHandler("robot:list", function (filter) {
        return API.getRobots(filter);
    });
});